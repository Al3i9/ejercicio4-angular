import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  dia : number = new Date().getDate();
  mes : number = new Date().getMonth();
  anio : number = new Date().getFullYear();
  constructor() { }

  ngOnInit(): void {
  }

}
